﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3__4__5
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountedItemBonus = 0.20;

        public DiscountedItem(IRentable rentable) : base(rentable)
        {

        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * this.DiscountedItemBonus;
        }

        public override String Description
        {
            get
            {
                return "Now at " + DiscountedItemBonus * 100 + "% off " + base.Description;
            }
        }
    }
}
