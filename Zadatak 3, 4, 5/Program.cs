﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3__4__5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> list1 = new List<IRentable>();
            Movie movie = new Movie("Joker");
            Book book = new Book("Orphan X");
            list1.Add(movie);
            list1.Add(book);
            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(list1);
            rent.PrintTotalPrice(list1);

            List<IRentable> list2 = new List<IRentable>();
            HotItem The_Outsider = new HotItem(new Book("The Outsider"));
            list2.Add(The_Outsider);
            HotItem Skyfall = new HotItem(new Movie("Skyfall"));
            list2.Add(Skyfall);
            RentingConsolePrinter Out2 = new RentingConsolePrinter();
            Out2.DisplayItems(list2);
            Out2.PrintTotalPrice(list2);

            List<IRentable> flashsale = new List<IRentable>();
            DiscountedItem low1 = new DiscountedItem(movie);
            DiscountedItem low2 = new DiscountedItem(book);
            flashsale.Add(low1);
            flashsale.Add(low2);
            RentingConsolePrinter Out3 = new RentingConsolePrinter();
            Out3.DisplayItems(flashsale);
            Out3.PrintTotalPrice(flashsale);
        }
    }
}
