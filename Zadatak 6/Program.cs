﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
            IEmailValidatorService emailValidator = new EmailValidator();
            IPasswordValidatorService passwordValidator = new PasswordValidator(8);
            string[] emails = { "mihovilkovacevic1998@gmail.com", "mihovilkovacevic1998@roccatkone", "ferit.hr" };
            string[] passwords = { "lozinka", "l00ZiNk44", "lozinka1998" };
            foreach (string email in emails)
            {
                bool isValid = emailValidator.IsValidAddress(email);
                Console.WriteLine('"' + email + "\" " + ((isValid) ? ("is") : ("is not")) + " a valid e-mail address.");
            }
            foreach (string password in passwords)
            {
                bool isValid = passwordValidator.IsValidPassword(password);
                Console.WriteLine('"' + password + "\" " + ((isValid) ? ("is") : ("is not")) + " a valid password.");
            }
        }
    }
}
