﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class UserEntry
    {
        public String Email { get; set; }

        public String Password { get; set; }

        private UserEntry()
        {

        }

        public static UserEntry ReadUserFromConsole()
        {
            UserEntry entry = new UserEntry();
            Console.WriteLine("Email: ");
            entry.Email = Console.ReadLine();
            Console.WriteLine("Password: ");
            entry.Password = Console.ReadLine();
            return entry;
        }
    }
}
