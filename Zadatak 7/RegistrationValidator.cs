﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class RegistrationValidator : IRegistrationValidator
    {
        public IEmailValidatorService emailValidator { get; set; }

        public IPasswordValidatorService passwordValidator { get; set; }

        public RegistrationValidator()
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(8);
        }

        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email)
                && passwordValidator.IsValidPassword(entry.Password);
        }
    }
}
