﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class Program
    {
        static void Main(string[] args)
        {
            IRegistrationValidator registrationValidator = new RegistrationValidator();
            while (true)
            {
                UserEntry userEntry = UserEntry.ReadUserFromConsole();
                if (registrationValidator.IsUserEntryValid(userEntry))
                    break;
                else
                    Console.WriteLine("Wrong input!");
            }
        }
    }
}
